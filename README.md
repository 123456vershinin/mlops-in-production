# MLOPS in production course

## How to install

For project installation you need *poetry* compatible with 1.8.0 version. You can find official installation guide [here](https://python-poetry.org/docs/#installing-with-the-official-installer). After just run `poetry install`

## Project methodology

For contributing you need to create pull request to **main** branch. You can create next types of pull requests:
1. Experiment pull request
2. Data pull request
3. Feature pull request
4. Configuration pull request
5. Bug pull request

### Experiment
Each experiment shouEach experiment should be carried out in single `.ipynb` file in [experiments](./experiments/) directory. Pull request should have *experiment/* prefix. After approving experiment correctness of this experiment, it will be merged to main. Experiment shouldn't make any changes in project source code.

Successful experiment can be transformed to *data* and *feature* pull request. Contributor should use *data* pull request if your experiment make some data transformations, and *feature* pull request if your experiment make modeling improvements.

### Data
This pull request need for adding data dependency to this project. Pull request should have *data/* prefix.

### Feature
This pull request need for adding some new or improving current functionality for this project. Pull request should have *feature/* prefix.

### Configuration
This pull request need for changing existing configuration for this project. Pull request should have *configuration/* prefix.

### Bug
This pull request need for fixing project bugs. Pull request should have *bug/* prefix.
